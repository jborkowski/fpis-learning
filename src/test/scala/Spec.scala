import jborkowski.laziness.Streamm

import collection.mutable.Stack
import org.scalatest._

class Spec extends FlatSpec with Matchers {

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new Stack[Int]
    stack.push(1)
    stack.push(2)
    stack.pop() should be (2)
    stack.pop() should be (1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[Int]
    a [NoSuchElementException] should be thrownBy {
      emptyStack.pop()
    }
  }

  "A List" should "contain new head value" in {
    val inputList = List_(1,2,3,4,5,6)
    val outputList = List_.setHead(inputList, 81)
    outputList should be (List_(81,2,3,4,5,6))
  }

  "A List" should "return a List without last element" in {
    val input = List_(1,2,3,4)
    val output = List_.init(input)
    output should be (List_(1,2,3))
  }

  "A new List" should "contain value increased by 1" in {
    import List_._
    val input = List_(1,2,3,4)
    add1(input) should be (List_(2,3,4,5))
  }

  "A list" should "convert list double to list string" in {
    import List_._
    val input = List_[Double](1.0, 1.1, 1.2)
    doubleToString(input) should be (List_("1.0", "1.1", "1.2"))
  }

  "A list" should "convert list double to string (map)" in {
    import List_._
    val input = List_[Double](1.2, 124.21)
    map(input)(_.toString) should be (List_("1.2", "124.21"))
  }

  "A list" should "contain only even numbers" in {
    import List_._
    val input = List_[Int](1,2,3,4,5,6)
    filter(input)(i => i % 2 == 0) should be (List_(2,4,6))
  }

  it should "return new flatMaped List" in {
    import List_._
    val input = List_[Int](1,2,3,4,5,6)
    flatMap(input)(i => List_(i,i)) should be (List_(1,1,2,2,3,3,4,4,5,5,6,6))
  }

  "A list" should "contain only even numbers (using FlatMap)" in {
    import List_._
    val input = List_[Int](1,2,3,4,5,6)
    filterFlatMap(input)(i => i % 2 == 0) should be (List_(2,4,6))
  }

  "A List" should "return list with added corresponding elements from input lists" in {
    import List_._
    val a1 = List_[Int](1,2,3,4,5,6)
    val a2 = List_[Int](1,2,3,4,5,6)
    addCorrespondingElements(a1,a2) should be (List_(2,4,6,8,10,12))
  }

  "A new List" should "contain multiplied pairwise elements form lists" in {
    import List_._
    val a1 = List_[Int](1,2,3,4)
    val a2 = List_[Int](2,1,3,4)
    zipWith(a1, a2)(_ * _) should be (List_(2,2,9,16))
  }

  val tree = Branch(Branch(Branch(Leaf(2), Branch(Leaf(1), Leaf(2))), Leaf(2)), Leaf(3))

  it should "count size of tree" in {
    Tree.size(tree) should be (9)
  }

  it should "return maximum value from tree" in {
    Tree.maximum(tree) should be (3)
  }

  it should "return maximum path from tree" in {
    Tree.depth(tree) should be (4)
  }

  it should "return new double tree" in {
    val output = Branch(Branch(Branch(Leaf(2.0), Branch(Leaf(1.0), Leaf(2.0))), Leaf(2.0)), Leaf(3.0))
    Tree.map(tree)(_.toDouble) should be (output)
  }

  it should "count size of tree (via fold)" in {
    Tree.sizeViaFold(tree) should be (9)
  }

  it should "return maximum value from tree (via fold)" in {
    Tree.maximumViaFold(tree) should be (3)
  }

  it should "return maximum path from tree (via fold)" in {
    Tree.depthViaFold(tree) should be (4)
  }

  it should "return new double tree (via fold)" in {
    val output = Branch(Branch(Branch(Leaf(2.0), Branch(Leaf(1.0), Leaf(2.0))), Leaf(2.0)), Leaf(3.0))
    Tree.mapViaFold(tree)(_.toDouble) should be (output)
  }



  it should "" in {
    import jborkowski.laziness._

    val s = Streamm[Int](1,2,3,4,5,6,7,8)
    s.take(2)

  }
}