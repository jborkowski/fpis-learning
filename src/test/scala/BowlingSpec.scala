import org.scalatest._

class BowlingSpec extends FlatSpec with Matchers {
  private var game: BowlingGame = new BowlingGame()

  override def withFixture(test: NoArgTest): Outcome = {
    game = new BowlingGame()

    super.withFixture(test)
  }

  private def rollMany(n: Int, pins: Int) =
    for (_ <- 1 to n)
      game roll(pins)

  private def rollSpare() =
    rollMany(2, 5)

  private def rollStrike() =
    game.roll(10)


  it should "play gutter game" in {
    rollMany(20, 0)
    game.score() should be (0)
  }

  it should "play only ones" in {
    rollMany(20, 1)
    game.score() should be (20)
  }

  it should "play with one Spare" in {
    rollSpare()
    game.roll(3)
    rollMany(17, 0)
    game.score() should be (16)
  }

  it should "play with one Strike" in {
    rollStrike()
    game.roll(3)
    game.roll(4)
    rollMany(16, 0)
    game.score() should be (24)
  }

  it should "play in perfect game" in {
    rollMany(12, 10)
    game.score() should be (300)
  }
}
