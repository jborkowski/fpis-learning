package basic


trait RNG {
  def nextInt: (Int, RNG)
}

object RNG {
  case class Simple(seed: Long) extends RNG {
    override def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
      val nextRNG = Simple(newSeed)
      val n = (newSeed >>> 16).toInt
      (n, nextRNG)
    }
  }

  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val n = Int.MaxValue + 1
    val (i, nextRng) = rng.nextInt
//    (i % n, nextRng)
    if (i < 0) (-i, nextRng) else (i, nextRng)
  }

  def double(rng: RNG): (Double, RNG) = {
    val (i, nextRng) = nonNegativeInt(rng)
    (i / (Int.MaxValue.toDouble + 1), nextRng)
  }

  def intDouble(rng: RNG): ((Int,Double), RNG) = {
    val (i, nextRng) = rng.nextInt
    val (d, nextRng2) = double(nextRng)
    ((i, d), nextRng2)
  }

  def doubleInt(rng: RNG): ((Double,Int), RNG) ={
    val ((i,d), nextRng) = intDouble(rng)
    ((d,i), nextRng)
  }

  def double3(rng: RNG): ((Double,Double,Double), RNG) = {
    val (d1, rng1) = double(rng)
    val (d2, rng2) = double(rng1)
    val (d3, rng3) = double(rng2)
    ((d1,d2,d3), rng3)
  }

  def inits(counts: Int)(rng: RNG): (List[Int], RNG) = {
    def go(cnt: Int, r: RNG, acc: List[Int]): (List[Int], RNG) = {
      if (cnt == 0) {
        (acc, r)
      } else {
        val (i, nextRng) = rng.nextInt
        go(cnt - 1, nextRng, i :: acc)
      }
    }
    go(counts, rng, List.empty[Int])
  }

  type Rand[+A] = RNG => (A, RNG)

  val int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] =
    rng => (a, rng)

  def map[A,B](s: Rand[A])(f: A => B): Rand[B] =
    rng => {
      val (a ,rng2) = s(rng)
      (f(a), rng2)
    }

  def nonNegativeEven: Rand[Int] =
    map(nonNegativeInt)(i => i - i % 2)

  def doubleMap: Rand[Double] =
    map(nonNegativeInt)(i => i / (Int.MaxValue.toDouble + 1))

  def map2[A,B,C](ra: Rand[A], rb: Rand[B])(f: (A,B) => C): Rand[C] =
    rng => {
      val (a ,rng2) = ra(rng)
      val (b ,rng3) = rb(rng2)
      (f(a,b), rng3)
    }

  def both[A,B](ra: Rand[A], rb: Rand[B]): Rand[(A,B)] =
    map2(ra, rb)((_,_))

  def randIntDouble: Rand[(Int,Double)] =
    both(int, doubleMap)

  def randDoubleInt: Rand[(Double,Int)] =
    both(doubleMap, int)

  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] =
    fs.foldRight(unit(List[A]()))((f, acc) => map2(f, acc)(_ :: _))

  def nonNegativeLessThan(n: Int): Rand[Int] = { rng =>
    val (i, rng2) = nonNegativeInt(rng)
    val mod = i % n
    if (i + (n - 1) - mod >= 0)
      (mod, rng2)
    else nonNegativeLessThan(n)(rng)
  }

  def flatMap[A,B](f: Rand[A])(g: A => Rand[B]): Rand[B] =
    rng => {
      val (s, rng2) = f(rng)
      g(s)(rng2)
    }

  def mapViaFlatMap[A,B](s: Rand[A])(f: A => B) =
    flatMap(s)(i => unit(f(i)))

  def map2ViaFlatMap[A,B,C](ra: Rand[A], rb: Rand[B])(f: (A,B) => C): Rand[C] =
    flatMap(ra)(a => map(rb)(b => f(a,b)))

  def rollDie: Rand[Int] =
    map(nonNegativeLessThan(6))(_ + 1)
}

import State._
case class State[S,+A](run: S => (A,S)) {
  def map[B](f: A => B): State[S, B] =
    flatMap(a => unit(f(a)))
  def map2[B,C](sb: State[S, B])(f: (A, B) => C): State[S, C] =
    flatMap(a => sb.map(b => f(a, b)))
  def flatMap[B](f: A => State[S, B]): State[S, B] = State(s => {
    val (a, s1) = run(s)
    f(a).run(s1)
  })
}

object State {
  type Rand[A] = State[RNG, A]

  def unit[S, A](a: A): State[S, A] =
    State(s => (a, s))

  def modify[S](f: S => S): State[S, Unit] = for {
    s <- get
    _ <- set(f(s))
  } yield ()

  def get[S]: State[S,S] = State(s => (s, s))

  def set[S](s: S): State[S,Unit] = State(_ => ((), s))

  def sequence[S, A](sas: List[State[S, A]]): State[S, List[A]] = {
    def go(s: S, actions: List[State[S,A]], acc: List[A]): (List[A],S) =
      actions match {
        case Nil => (acc.reverse,s)
        case h :: t => h.run(s) match { case (a,s2) => go(s2, t, a :: acc) }
      }
    State((s: S) => go(s,sas,List()))
  }
}

sealed trait Input
case object Coin extends Input
case object Turn extends Input

case class Machine(locked: Boolean, candies: Int, coins: Int)

object Candy {
  def update = (i: Input) => (s: Machine) => (i, s) match {
    case (_, Machine(_, 0, _))                => s
    case (Coin, Machine(false, _, _))         => s
    case (Turn, Machine(true, _, _))          => s
    case (Coin, Machine(true, candy, coin))   => Machine(false, candy, coin + 1)
    case (Turn, Machine(false, candy, coin))  => Machine(true, candy - 1, coin)
  }

  def simulateMachine(input: List[Input]): State[Machine, (Int, Int)] = for {
    _ <- sequence(input map (modify[Machine] _ compose update))
    s <- get
  } yield (s.coins, s.candies)
}
