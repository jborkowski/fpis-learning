package basic.l
import scala.annotation.tailrec

import Stream._

trait Stream[+A] {
  import Stream._

  def toList: List[A] = {
    @tailrec
    def go(s: Stream[A], acc: List[A]): List[A] = s match {
      case Cons(hd, tl) => go(tl(), hd() :: acc)
      case _ => acc
    }
    go(this, List()).reverse
  }

  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 1  => cons(h(), t().take(n - 1))
    case Cons(h, _) if n == 0 => cons(h(), empty)
    case _          => empty
  }

  @annotation.tailrec
  final def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if n > 0 => t().drop(n - 1)
    case _ => this
  }

  def exist(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) || t().exist(p)
    case _ => false
  }

  def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
    case Cons(h, t) => f(h(), t().foldRight(z)(f))
    case _ => z
  }

  def existFoldRight(p: A => Boolean): Boolean =
    foldRight(false)((a,b) => p(a) || b)

  def forAll(p: A => Boolean): Boolean =
    foldRight(true)((a,b) => p(a) && b)

  def takeWhile(p: A => Boolean): Stream[A] =
    foldRight(Stream[A]())((a, acc) =>
      if (p(a)) cons(a, acc)
      else empty
    )

  def headOption: scala.Option[A] =
    foldRight(None: scala.Option[A])((h, _) => scala.Option(h))

  def map[B](f: A => B): Stream[B] =
    foldRight(Empty: Stream[B])((h, acc) => cons(f(h), acc))

  def filter(p: A => Boolean): Stream[A] =
    foldRight(Empty: Stream[A])((h, acc) =>
    if (p(h)) cons(h, acc)
    else empty)

  def append(e: => Stream[A]): Stream[A] =
    foldRight(this)((h, acc) => cons(h, acc))

  def flatMap[B](f: A => Stream[B]): Stream[B] =
    foldRight(Empty: Stream[B])((h, t) => f(h) append t)

  def find(p: A => Boolean): scala.Option[A] =
    filter(p).headOption

  def takeWile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if (h()) => cons(h(), t() takeWile p)
    case _ => empty
  }

  def mapViaUnfold[B](f: A => B): Stream[B] =
    unfold(this) match {
      case Cons(h,t) => Some((f(h()), t()))
      case _ => None
    }

  def takeViaUnfold(n: Int): Stream[A] =
    unfold((this, n)) match {
      case (Cons(h,t), 1) => Some(h(), (empty, 0))
      case (Cons(h,t), 0) if n > 1 => Some(h(), (t(), n-1))
      case _ => None
    }

  def takeWhileViaUnfold(p: A => Boolean): Stream[A] =
    unfold(this) match {
      case Cons(h,t) if p(h()) => Some((h(), t()))
      case _ => None
    }

  def zipWith[B,C](s2: Stream[B])(f: (A,B) => C): Stream[C] =
    unfold(this, s2) match {
      case (Cons(h1, t1), Cons(h2, t2)) => Some((f(h1(), h2()), (t1(), t2())))
      case _ => None
    }

  def zipAll[B](s2: Stream[B]): Stream[(Option[A], scala.Option[B])] =
    zipWithAll(s2)((_,_))


  def zipWithAll[B, C](s2: Stream[B])(f: (Option[A], scala.Option[B]) => C): Stream[C] =
    Stream.unfold((this, s2)) {
      case (Empty, Empty) => None
      case (Cons(h, t), Empty) => Some(f(Some(h()), scala.Option.empty[B]) -> (t(), empty[B]))
      case (Empty, Cons(h, t)) => Some(f(scala.Option.empty[A], Some(h())) -> (empty[A] -> t()))
      case (Cons(h1, t1), Cons(h2, t2)) => Some(f(Some(h1()), Some(h2())) -> (t1() -> t2()))
    }

  def startWith[A](s: Stream[A]): Boolean =
    zipAll(s).takeWhileViaUnfold(!_._2).forAll {
      case (sh1, sh2) => sh1 == sh2
    }
  def tails: Stream[Stream[A]] =
    unfold(this) match {
      case empty => None
      case s => Some((s, s drop 1))
    } append Stream.empty

  def hasSubsequence[A](s: Stream[A]): Boolean =
    tails exist (_ startWith s)

  def scanRight[B](z: B)(f: (A, => B) => B): Stream[B] =
    foldRight((z, Stream(z)))((a, p0) => {
      lazy val p1 = p0
      val b2 = f(a, p1._1)
      (b2, cons(b2, p1._1))
    })._2
}
case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }

  def empty[A]: Stream[A] = Empty

  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))

  def constant[A](a: A): Stream[A] = {
    lazy val tail = Stream.cons(a, tail)
    tail
  }

  def from(n: Int): Stream[Int] =
    Stream.cons(n, from(n+1))

  def fibs: Stream[Int] = {
    def go(e0: Int, e1: Int): Stream[Int] = {
      Stream.cons(e0, go(e1, e0+e1))
    }
    go(0,1)
  }

  def unfold[A, S](z: S)(f: S => scala.Option[(A, S)]): Stream[A] = f(z) match {
    case Some((h,s)) => cons(h, unfold(s)(f))
    case None => empty
  }

  def fibsViaUnfold: Stream[Int] =
    unfold((0,1)) { case (e0,e1) => Some((e0,(e1,e0+e1))) }

  def fromViaUnfold(n: Int): Stream[Int] =
    unfold(n)(n => Some((n, n+1)))

  def constantViaFold[A](a: A) =
    unfold(a)(a => Some((a, a)))

  def onesViaFold =
    unfold(1)(_ => Some((1,1)))
}

