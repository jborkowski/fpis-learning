package basic

import java.util.NoSuchElementException
import scala.annotation.tailrec

object Chapter3 extends App {

  val matchResult = List_(1,2,3,4,5) match {
    case Cons_(x, Cons_(2, Cons_(4, _))) => x
    case Nil => 42
    case Cons_(x, Cons_(y, Cons_(3, Cons_(4, _)))) => x + y
    case Cons_(h, t) => h + List_.sum(t)
    case _ => 101
  }
  println(matchResult)

  println(List_.foldRight(List_(1,2,3), Nil:List_[Int])(Cons_(_, _)))

  println(List_.length(List_(1,2,3,4,5,6)))
  println(List_.length2(List_(1,2,3,4,5,6)))
  println(List_.foldLeft(List_(1,2,3,4,5,6), 0)(_+_))
  println(List_.reverse(List_(1,2,3,4,5,6)))
  println(List_.appendFoldRight(List_(1,2), List_(3,4)))
}

sealed trait List_[+A]
case object Nil extends List_[Nothing]
case class Cons_[+A](h: A, t: List_[A]) extends List_[A]

object List_ {
  def sum(l: List_[Int]): Int = l match {
    case Nil         => 0
    case Cons_(x, xs) => x + sum(xs)
  }

  def tail[A](l: List_[A]): List_[A] = l match {
    case Nil        => Nil
    case Cons_(_, t) => t
  }

  def head[A](l: List_[A]): A = l match {
    case Nil => throw new NoSuchElementException("Nil")
    case Cons_(h, _) => h
  }

  def setHead[A](l: List_[A], h: A): List_[A] = l match {
    case Nil => Nil
    case Cons_(_, t) => Cons_(h, t)
  }

  def drop[A](l: List_[A], n: Int): List_[A] =
    if (n <= 0) l
    else drop(List_.tail(l), n - 1)

  def dropWhile[A](l: List_[A], p: A => Boolean): List_[A] =
    if (!p(List_.head(l))) l
    else dropWhile(List_.tail(l), p)

  def append[A](a1: List_[A], a2: List_[A]): List_[A] = a1 match {
    case Nil => a2
    case Cons_(h, t) => Cons_(h, append(t, a2))
  }

  def init[A](l: List_[A]): List_[A] = {
    def go(l: List_[A], acc: List_[A]): List_[A] = l match {
      case Nil => Nil
      case Cons_(h, Cons_(_, Nil)) => List_.append(acc, List_(h))
      case Cons_(h, t) => go(List_.tail(l), List_.append(acc, List_(h)))
    }

    go(l, List_())
  }

  def foldRight[A, B](as: List_[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons_(x, xs) => f(x, foldRight(xs, z)(f))
  }

  def sum2(l: List_[Int]) =
    foldRight(l, 0)(_ + _)

  def prod2(l: List_[Double]) =
    foldRight(l, 0.0)(_ * _)

  def length[A](as: List_[A]) =
    foldRight(as, 0)((_, acc) => acc + 1)

  @tailrec
  def foldLeft[A,B](as: List_[A], z: B)(f: (B, A) => B): B = as match {
    case Nil         => z
    case Cons_(x, xs) => foldLeft(xs, f(z, x))(f)
  }

//  @tailrec
//  def go(l: List[A], acc: B): B = l match {
//    case Nil         => acc
//    case Cons(x, xs) => go(xs, f(acc, x))
//  }

  def sum3(as: List_[Int]): Int =
    foldLeft(as, 0)(_ + _)

  def prod3(as: List_[Double]): Double =
    foldLeft(as, 0.0)(_ * _)

  def length2[A](as: List_[A]) =
    foldLeft(as, 0)((acc, _) => acc + 1)

  def reverse[A](as: List_[A]) =
    foldLeft(as, Nil: List_[A])((acc, item) => Cons_(item, acc))

  def foldRightViaFoldLeft[A,B](as: List_[A], z: B)(f: (A, B) => B): B =
    foldLeft(reverse(as), z)((acc, h) => f(h, acc))

  def appendFoldRight[A](a1: List_[A], a2: List_[A]): List_[A] =
    foldRight(a1, a2)(Cons_(_, _))

  def concat[A](as: List_[List_[A]]): List_[A] =
    foldRight(as, List_(): List_[A])(append)

  def add1(as: List_[Int]): List_[Int] =
    foldRight(as, Nil: List_[Int])((i, acc) => Cons_(i + 1, acc))

  def doubleToString(input: List_[Double]) =
    foldRight(input, Nil: List_[String])((i, acc) => Cons_(i.toString, acc))

  def map[A,B](as: List_[A])(f: A => B): List_[B] =
    foldRight(as, Nil: List_[B])((i, acc) => Cons_(f(i), acc))

  def filter[A](as: List_[A])(f: A => Boolean): List_[A] =
    foldRight(as, Nil: List_[A])((h, t) => if (f(h)) Cons_(h, t) else t )

  def flatMap[A,B](as: List_[A])(f: A => List_[B]): List_[B] =
    foldRight(as, Nil: List_[B])((h, t) => append(f(h), t))

  def filterFlatMap[A](as: List_[A])(f: A => Boolean): List_[A] =
    flatMap(as)(i => if (f(i)) List_(i) else Nil)

  def addCorrespondingElements(a1: List_[Int], a2: List_[Int]): List_[Int] = (a1, a2) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons_(ah, at), Cons_(bh, bt))   => Cons_(ah+bh, addCorrespondingElements(at, bt))
  }

  def zipWith[A,B,C](a1: List_[A], a2: List_[B])(f: (A,B) => C): List_[C] = (a1, a2) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons_(ah, at), Cons_(bh, bt)) => Cons_(f(ah, bh), zipWith(at, bt)(f))
  }

  def apply[A](as: A*): List_[A] =
    if (as.isEmpty) Nil
    else Cons_(as.head, apply(as.tail: _*))

}

sealed trait Tree[+A]
case class Leaf[A](value: A) extends Tree[A]
case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {
  def size[A](t: Tree[A]): Int = t match {
    case Leaf(_) => 1
    case Branch(l, r) => 1 + size(l) + size(r)
  }

  def maximum(t: Tree[Int]): Int = t match {
    case Leaf(value) => value
    case Branch(left, right) => maximum(left) max maximum(right)
  }

  def depth[A](t: Tree[A]): Int = t match {
    case Leaf(_) => 0
    case Branch(l, r) => 1 + (depth(l) max depth(r))
  }

  def map[A,B](t: Tree[A])(f: A => B): Tree[B] = t match {
    case Leaf(v) => Leaf(f(v))
    case Branch(l, r) => Branch(map(l)(f), map(r)(f))
  }

  def fold[A,B](t: Tree[A])(f: A => B)(g: (B,B) => B): B = t match {
    case Leaf(v) => f(v)
    case Branch(l, r) => g(fold(l)(f)(g), fold(r)(f)(g))
  }

  def sizeViaFold[A](t: Tree[A]) =
    fold(t)(a => 1)(1 + _ + _)

  def maximumViaFold(t: Tree[Int]): Int =
    fold(t)(a => a)(_ max _)

  def depthViaFold[A](t: Tree[A]) =
    fold(t)(a => 0)((b1, b2) => 1 + (b1 max b2))

  def mapViaFold[A,B](t: Tree[A])(f: A => B) =
    fold(t)(a => Leaf(f(a)): Tree[B])(Branch(_, _))
}