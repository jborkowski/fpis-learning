import scala.collection.mutable.ArrayBuffer

object Bowling {
  def roll(int: Int) = ???
  def score(): Int = ???
}

class BowlingGame {
  private val rolls: ArrayBuffer[Int] = ArrayBuffer.fill(21){0}
  private var currentRoll = 0

  def roll(pins: Int) = {
    rolls (currentRoll) = pins
    currentRoll += 1
  }

  def score(): Int = {
    var score: Int = 0
    var firstInFrame = 0
    for (frame <- 1 to 10) {
      if (isStrike(firstInFrame)) {
        score += (10 + getTwoBallsForStrike(firstInFrame))
        firstInFrame += 1
      } else if (isSpare(firstInFrame)) {
        score += 10 + getNextBallForSpare(firstInFrame)
        firstInFrame += 2
      } else {
        score += (rolls(firstInFrame) + rolls(firstInFrame + 1))
        firstInFrame += 2
      }
    }
    score
  }

  def isSpare(index: Int): Boolean =
    rolls(index) + rolls(index + 1) == 10

  def isStrike(index: Int): Boolean =
    rolls(index) == 10

  def getTwoBallsForStrike(index: Int): Int =
    rolls(index + 1) + rolls(index + 2)

  def getNextBallForSpare(index: Int): Int =
    rolls(index + 2)

}
