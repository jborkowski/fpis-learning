package basic

import scala.annotation.tailrec

object Main extends App {
  import MyModule._

  println(formatAbs(-42))
  println(formatFactorial(7))
  println(formatFibonacci(4))
  println(findFirst[Int](Array(1,2,3,4,5), i => i == 4))

  println(isSorted[Int](Array(1,2,3,4,5), _ < _))

  def first[A, B](x: A, y: B): A = x
  def As[A](x: A): A = x
  println(curry(first[Int, Int])(1)(3))
  //println(uncurry[Int, Int, Int])

}


/** Documentation comment */
object MyModule {
  def abs(n: Int): Int =
    if (n < 0) -n else n

  def factorial(n: Int): Int = {
    @tailrec
    def go(n: Int, acc: Int): Int =
      if (n <= 0) acc
      else go(n - 1, acc * n)

    go(n, 1)
  }

  def fibonacci(n: Int): Int = {
    if (n == 0 | n == 1) n
    else fibonacci(n - 2) + fibonacci(n - 1)
  }

  def findFirst[A](as: Array[A], p: A => Boolean): Int = {
    @tailrec
    def loop(n: Int): Int =
      if (n >= as.length) -1
      else if (p(as(n))) n
      else loop(n + 1)

    loop(0)
  }

  def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean = {
    @tailrec
    def loop(a: Array[A]): Boolean =
      if (a.length > 0) true
      else if (ordered(a.head, a.tail.head)) loop(a.tail)
      else false

    loop(as)
  }

  def partial1[A, B, C](a: A, f: (A, B) => C): B => C =
    (b: B) => f(a, b)

  def curry[A, B, C](f: (A, B) => C): A => (B => C) =
    (a: A) => (b:B) => f(a,b)

  def uncurry[A, B, C](f: A => B => C): (A, B) => C =
    (a: A, b:B) => f(a)(b)

  def compose[A, B, C](f: B => C, g: A => B): A => C =
    (a: A) => f(g(a))

  def formatAbs(x: Int) =
    formatResult("absolute", x, abs)

  def formatFactorial(x: Int) =
    formatResult("factorial", x, factorial)

  def formatFibonacci(x: Int) =
    formatResult("fibonacci", x, fibonacci)

  def formatResult(name: String, n: Int, f: Int => Int) =
    s"The $name of $n is ${f(n)} "
}
